
# Grunt Lab



## create npm project

#### create a new folder and move to it and open it in sublime
	$ mkdir FOLDERNAME
	$ cd FOLDERNAME
	$ subl . -a

#### initiate a npm project
	$ npm init // and follow instructions


## git the shit out that project

#### install atlassian source three (or other) or just the git cli
[source three](https://www.sourcetreeapp.com/)

[git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

#### create a github or BitBucket account and configure your git ui/cli with a remote account
more [here](https://confluence.atlassian.com/bitbucketserver/set-up-sourcetree-to-work-with-bitbucket-server-776798217.html)

#### clone/create a git rep
	$ git clone https://PATH/TO/REPO.git
or use Ui app

#### make a readme.md file and open it in sublime (then obviously write something in it)
	$ touch readme.md; subl readme.md -a // -a is to no open a new window (a => add)

#### make you first commit
	$ git commit
or use Ui app




[why](https://medium.com/javascript-scene/software-versions-are-broken-3d2dc0da0783#.mzwhdzsdf) the "--save-exact" argument ?


#### instal foundations (method CLI) (make global so you'll arleady have it for otherp project)
	$ npm install --global --save-exact  foundation-cli


#### install foundations (method UI)

download app [here](http://foundation.zurb.com/develop/yeti-launch)


## Make a website using foundations

## install grunt

#### instal the folowing node package in global : w/ arg "-g" and save config
	$ npm install -g --save --save-exact PACKAGENAME
+ grunt


#### instal the folowing node package in devDependency mode : w/ arg "---save-dev"

	$ npm install --save-dev  --save-exact  PACKAGENAME PACKAGENAME
	// OR : instal multiple package at once and use install shortcut command 'i'
	$ npm i --save-dev  --save-exact  PACKAGENAME PACKAGENAME PACKAGENAME
+ grunt-contrib-clean
+ grunt-contrib-concat
+ grunt-contrib-min
+ grunt-contrib-copy
+ grunt-contrib-watch


## configure grunt

#### make a grunt file

##### create a grunt.js file a the root of the npm project and open it in sublime

	$ touch grunt.js; subl grunt.js -a

##### check the GODDAMMMN doc


##### write a config and create grunt command(s) by concatening command(s)
+ minify all css files
+ minify all js files
+ create concatenated grunt command to minify everything (js and css)
+ concatenate all the .css files produced by grunt minification (in the desired order!!!)
+ concatenate all javascript file into one

+ (make a custom plugins to, why not, take care of cahnging some specific variables that are different in prod and dev env.)

##### create npm script(s) (runDev,runProd,runCss,runJs) to easily concatenate/combine grunt,foundations and bash command (!!! platforms' shell synthax)








